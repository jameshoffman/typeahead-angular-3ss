import { Component } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { User } from './user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  users = [
    new User({ id: 1, pseudo: 'j_doe', firstname: 'John', lastname: 'Doe'}),
    new User({ id: 2, pseudo: '__mike__', firstname: 'Magic', lastname: 'Mike'})
    ,
    new User({ id: 3, pseudo: 'THE_GOAT', firstname: 'Chuck', lastname: 'Norris'})
  ]

  form: FormGroup;
  selected: User | null = null;

  constructor() {
    this.form = new FormGroup({
      user: new FormControl('', [Validators.required, ((control: AbstractControl): ValidationErrors  => {
        return this.selected == null ? { invalid: true } : null;
      })])
    });
  }

  changed(status: any) {
    if (typeof(status) == 'boolean' && status == true) {
      console.log('Empty?', status);
      this.selected = null;
    }

    if (typeof(status) == 'string' && status != this.selected?.displayName) {
      console.log('Modified?', status);
      this.selected = null;
    }

    if (status?.item instanceof User) {
      console.log('Selected', status.item);
      this.selected = status.item;
    }

    this.form.get('user')?.updateValueAndValidity();
  }
}
