export class User {
  id!: number;
  pseudo!: string;
  firstname!: string;
  lastname!: string;

  constructor(json: any) {
    Object.assign(this, json);
  }

  get displayName() {
    return `${this.firstname} ${this.lastname}, ${this.pseudo}`;
  }
}
